from flask import Flask, request, jsonify
from pymongo import MongoClient
from bson import ObjectId

app = Flask(__name__)

uri = "mongodb://root:root@localhost:27017/"

client = MongoClient(uri)


db = client.filmes

filmes = db.filmes_americanos

# '_id': i['nome_avaliacao'],
# "titulo_filme": i['titulo_filme'],
# "nota": i['nota'],
# 'autor': i['autor'],
# 'comentario': i['comentario']



@app.get('/filme')
def get_filmes():
    valores = request.get_json()
    data = filmes.find(valores)
    retorno = []
    data = list(data)
    for i in data:
        dados = {
            'nome_avaliacao': i['nome_avaliacao'],
            "titulo_filme": i['titulo_filme'],
            "nota": i['nota'],
            'autor': i['autor'],
            'comentario': i['comentario']
        }
        
        retorno.append(dados)
    
    return jsonify(retorno)

@app.post('/filme')
def post_filme():
    data = request.get_json()
    filmes.insert_many(data)

    ids= []

    for i in data:
        ids.append(i['nome_avaliacao'])

    return jsonify(ids), 201 

@app.put('/filme')
def put_filme():
    data = request.get_json()
    query = data['query']
    update = data['update']

    filmes.update_one(query,{'$set': update})
    atualizado = filmes.find(query)
    atualizado = list(atualizado)[0]
    dados = {
            'nome_avaliacao': atualizado['nome_avaliacao'],
            "titulo_filme": atualizado['titulo_filme'],
            "nota": atualizado['nota'],
            'autor': atualizado['autor'],
            'comentario': atualizado['comentario']
        }

    return jsonify(dados), 204

@app.delete('/filme')
def delete_filme():
    data = request.get_json()

    atualizado = filmes.find(data)
    atualizado = list(atualizado)[0]
    dados = {
            'nome_avaliacao': atualizado['nome_avaliacao'],
            "titulo_filme": atualizado['titulo_filme'],
            "nota": atualizado['nota'],
            'autor': atualizado['autor'],
            'comentario': atualizado['comentario']
        }
    
    filmes.delete_one(data)

    return jsonify(dados), 200


app.run()