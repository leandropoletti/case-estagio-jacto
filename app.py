from os import getenv
from dotenv import load_dotenv
import psycopg2
from flask import Flask, request, jsonify

load_dotenv()

app = Flask(__name__,)
dbName = getenv("DATABASE_NAME")
dbUser = getenv("DATABASE_USER")
dbPassword = getenv("DATABASE_PASSWORD")
dbHost = getenv("DATABASE_HOST")
dbPort = getenv("DATABASE_PORT")

connection = psycopg2.connect(database=dbName, user=dbUser, password=dbPassword, host=dbHost, port = dbPort)

CREATE_FABRICANTES_TABLE = (
    """
    CREATE TABLE IF NOT EXISTS fabricantes (id SERIAL PRIMARY KEY, name TEXT UNIQUE NOT NULL);
    """
)

CREATE_CARS_TABLE = (
    """
    CREATE TABLE IF NOT EXISTS cars(
    id SERIAL PRIMARY KEY,
    nome TEXT NOT NULL,
    ano INTEGER,
    fabricanteId INTEGER NOT NULL,
    FOREIGN KEY(fabricanteId) references fabricantes(id) ON DELETE CASCADE
    );
    """
)

INSERT_INTO_FABRICANTES = "INSERT INTO fabricantes (name) VALUES (%s) RETURNING id;"
INSERT_INTO_CARS = "INSERT INTO cars(nome, ano, fabricanteId) VALUES (%s, %s, %s) RETURNING id;"
SELECT_FABRICANTE = "SELECT * FROM fabricantes WHERE fabricantes.id = %s"
SELECT_CAR = "SELECT * FROM cars JOIN fabricantes on cars.fabricanteId = fabricantes.id WHERE cars.id = %s"
UPDATE_FABRICANTE = "UPDATE fabricantes SET name = %s WHERE id = %s"
UPDATE_CAR = "UPDATE cars SET nome = %s, ano = %s, fabricanteId = %s WHERE id = %s"
DELETE_FABRICANTE = "DELETE FROM fabricantes where id = %s"
DELETE_CAR = "DELETE FROM cars WHERE id = %s"


@app.get('/api/fabricantes/<fabricanteId>')
def get_fabricante(fabricanteId):
    parameters = fabricanteId,
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(SELECT_FABRICANTE, parameters)
            data = cursor.fetchall()
    
    values = {
        'id': data[0][0],
        'nomeFabricante': data[0][1]
    }

    return jsonify(values), 200

@app.get('/api/cars/<carId>')
def get_car(carId):
    parameters = carId,
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(SELECT_CAR, parameters)
            data = cursor.fetchall()
    
    values = {
        'id': data[0][0],
        'nome': data[0][1],
        'ano': data[0][2],
        'idFabricante': data[0][3],
        'nomeFabricante': data[0][5]
    }

    return jsonify(values), 200

@app.post('/api/fabricantes')
def insert_fabricante():
    data = request.get_json()
    name = data['nomeFabricante']
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(CREATE_FABRICANTES_TABLE)
            cursor.execute(INSERT_INTO_FABRICANTES, (name,))
            fabricanteId = cursor.fetchone()[0]
            idReturn = {'id': fabricanteId,
                        'nome': name}
    
    return jsonify(idReturn), 201

@app.post('/api/cars')
def insert_car():
    data = request.get_json()
    nome = data['nomeCarro']
    try:
        ano = data['ano']
    except KeyError:
        ano = "0" 
    
    fabricanteId = data['fabricanteId']

    parameters = nome,ano,fabricanteId

    with connection:
        with connection.cursor() as cursor:
            cursor.execute(CREATE_CARS_TABLE)
            cursor.execute(INSERT_INTO_CARS, parameters)
            carroId = cursor.fetchone()[0]
            

    return jsonify(carroId), 201

@app.put('/api/fabricantes')
def put_fabricante():
    data = request.get_json()
    updateId = data['id']
    novoNome = data['nomeFabricante']
    parameters = (novoNome, updateId)

    with connection:
        with connection.cursor() as cursor:
            cursor.execute(UPDATE_FABRICANTE, parameters)

    return jsonify('Update successful'),204

@app.put('/api/cars')
def put_car():
    data = request.get_json()
    updateId = data['id']
    novoNome = data['nome']
    try:
        novoAno = data['ano']
    except KeyError:
        novoAno = 0
    novoFabricante = data['idFabricante']
    parameters = (novoNome, novoAno, novoFabricante, updateId)

    with connection:
        with connection.cursor() as cursor:
            cursor.execute(UPDATE_CAR, parameters)

    return jsonify('Update successful'),204

@app.delete('/api/fabricante/<fabricanteId>')
def delete_fabricante(fabricanteId):
    parameters = fabricanteId,
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(DELETE_FABRICANTE, parameters)
    return jsonify("Deleted succesfully"), 204

@app.delete('/api/cars/<carId>')
def delete_cars(carId):
    parameters = carId,
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(DELETE_CAR, parameters)
    return jsonify("Deleted succesfully"), 204


app.run(host='0.0.0.0', port=8000, debug=getenv("FLASK_DEBUG"))
