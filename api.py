from os import getenv
from dotenv import load_dotenv
from openai import OpenAI
from flask import Flask, request, jsonify

load_dotenv()
personal_api_key = getenv("OPENAI_API_KEY")

app = Flask(__name__)

client = OpenAI(api_key=personal_api_key)


@app.get('/api')
def send_prompt():
    query = request.get_json()['prompt']

    completion = client.chat.completions.create(
    model = "gpt-3.5-turbo",
    messages= [
        {"role": "user", "content": query}
    ]
)   
    
    answer = {
        'output': completion.choices[0].message.content
    }

    return jsonify(answer), 200


app.run()
